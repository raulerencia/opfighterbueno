﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelotaController : MonoBehaviour
{
    
    GameObject luffy;
    GameObject sanji;

    // Awake is called on object instantiation
    void Awake()
    {

        luffy = GameObject.Find("Luffy");

        sanji = GameObject.Find("Sanji");

        //posiciona la pelota al centro de la pantalla
        this.transform.position = new Vector2(0,3);

    }



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //se destruye al colisionar
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag.Equals("SanjiBody"))
        {
            Destroy(this.gameObject);
        }
        else if (collision.transform.tag.Equals("LuffyBody"))
        {
            Destroy(this.gameObject);
        }
    }

}
