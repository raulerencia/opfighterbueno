﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SanjiAttack : MonoBehaviour
{

    GameObject luffy;
    GameObject sanji;
   
    public GameObject luffyLife;
    float vidaLuffy;

    // Start is called before the first frame update
    void Start()
    {
        luffy = GameObject.Find("Luffy");
       
        sanji = GameObject.Find("Sanji");
        //se suma al evento
        luffy.GetComponent<PlayerController>().eventAtaqueLuffy += perderVidaLuffy;
        vidaLuffy = luffyLife.GetComponent<Image>().fillAmount;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void perderVidaLuffy()
    {
        //resta el valor de la variable para que el tamaño de ola imagen de la vida, se reduzca.
        vidaLuffy -= 0.05f;
        luffyLife.GetComponent<Image>().fillAmount = vidaLuffy;
    }
}
