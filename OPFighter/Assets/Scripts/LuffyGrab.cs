﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuffyGrab : MonoBehaviour
{
    GameObject luffy;

    // Start is called before the first frame update
    void Start()
    {
        luffy = GameObject.Find("Luffy");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //empuja el rival
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("SanjiBody"))
        {
            if(luffy.GetComponent<PlayerController>().side == true)
            {
                collision.GetComponentInParent<Rigidbody2D>().AddForce(new Vector2(500, 800));
            }
            else
            {
                collision.GetComponentInParent<Rigidbody2D>().AddForce(new Vector2(-500, 800));
            }
            
            
        }
    }
}
