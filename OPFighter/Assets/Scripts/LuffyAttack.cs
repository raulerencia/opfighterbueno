﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LuffyAttack : MonoBehaviour
{
    GameObject luffy;
    GameObject sanji;
    public GameObject sanjiLife;
    float vidaSanji;

    // Start is called before the first frame update
    void Start()
    {
        luffy = GameObject.Find("Luffy");
        sanji = GameObject.Find("Sanji");
        //se suma al evento
        sanji.GetComponent<SanjiController>().eventAtaqueSanji += perderVidaSanji;
        vidaSanji = sanjiLife.GetComponent<Image>().fillAmount;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //resta el valor de la variable para que el tamaño de ola imagen de la vida, se reduzca.
    public void perderVidaSanji()
    {
        vidaSanji -= 0.05f;
        sanjiLife.GetComponent<Image>().fillAmount = vidaSanji;
    }
}
