﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFight : MonoBehaviour
{
    //activa y desactiva el texto de fight al inicio del compate

    // Start is called before the first frame update
    void Start()
    {
        
        this.gameObject.GetComponent<Image>().enabled = true;

        Invoke("deletefFight", 2);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void deletefFight()
    {
        this.gameObject.GetComponent<Image>().enabled = false;
    }

}
