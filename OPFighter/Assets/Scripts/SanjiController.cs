﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SanjiController : MonoBehaviour
{
    //evento para poder restar la vida del marcador al recibir daño
    public delegate void ataqueSanji();
    public event ataqueSanji eventAtaqueSanji;

    [Header("Valores")]
    public GameObject pj;
    public int velocity;
    public int jumpForce;
    public GameObject life;

    [Header("Keys")]
    public KeyCode right;
    public KeyCode left;
    public KeyCode jump;
    public KeyCode hit;
    public KeyCode combo;
    public KeyCode guard;
    public KeyCode fire;

    [Header("Booleans")]
    public bool isGrounded;
    public bool side = true;
    private bool fireTime = false;
    public bool isGrabbing = false;
    private bool cBreak = false;
    private bool died = false;

    [Header("Animaciones")]
    public Animator animator;
    
    public GameObject fireball;
    public GameObject pelota;
    GameObject p;
    private int status = 0;
    private int st = 1;


    // Start is called before the first frame update
    void Start()
    {
        animator.SetTrigger("hit");
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        //activa la animacion de protegerse y desactiva la hurtbox
        if (Input.GetKey(guard) && !isGrabbing && !died)
        {
            animator.SetBool("guard", true);
        }
        //da un golpe, esto activa dos corutinas, una para hacer el combo despues de 0,4seg, y cancelarlo despues de 1seg
        else if (Input.GetKeyDown(hit) && !isGrabbing && !died)
        {
            animator.SetTrigger("hit");
            StartCoroutine(ComboHit(0.4f));
            StartCoroutine(Combobreak(1f));
        }
        //activa el combo si esta dentro del tiempo
        else if (Input.GetKeyDown(combo) && isGrounded && !died)
        {
            if (status == st)
            {
                animator.SetTrigger("pirueta");
            }
        }
        //instancia una bola de fuego que lanzara hacia adelante
        else if (Input.GetKeyDown(fire) && !fireTime && !died)
        {
            animator.SetTrigger("fireball");
            Instantiate(fireball);
            fireTime = true;
            StartCoroutine(FireTimeFalse(1.5f));

        }
        //movimientos laterales.
        else if (Input.GetKey(right) && !isGrabbing && !died)
        {
            if (isGrounded)
            {
                animator.SetBool("jump", false);
                animator.SetBool("left", false);
            }
            if (side == false)
            {
                side = true;

                this.transform.Rotate(0, 180, 0);
            }

            this.transform.Translate(new Vector3(0.1f, 0, 0));

            animator.SetBool("right", true);
        }
        else if (Input.GetKey(left) && !isGrabbing && !died)
        {
            if (isGrounded)
            {
                animator.SetBool("right", false);
                animator.SetBool("jump", false);
            }
            if (side == true)
            {
                side = false;

                this.transform.Rotate(0, 180, 0);
            }

            this.transform.Translate(new Vector3(0.1f, 0, 0));
            animator.SetBool("left", true);
        }
        //activa el salto y desactiva animaciones
        else if (Input.GetKey(jump) && isGrounded && !isGrabbing && !died)
        {
            animator.SetBool("right", false);
            animator.SetBool("left", false);
            animator.SetBool("jump", true);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
            isGrounded = false;
        }
        //si ha muerto carga la pantalla final
        else if (died)
        {
            animator.SetBool("died", true);
            Invoke("cargarFinal", 3);
           
        }
        //si no tiene vida, activa el bool "died"
        else if (life.GetComponent<Image>().fillAmount == 0)
        {
            animator.SetBool("die", true);
            if (side == true)
            {
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200, 700));
            }
            else
            {
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(200, 700));
            }

            died = true;

        }
        else
        {//desactiva animaciones
            if (isGrounded)
            {
                animator.SetBool("right", false); animator.SetBool("jump", false); animator.SetBool("guard", false); animator.SetBool("left", false);
            }

        }
    }
    //le da un tiempo de cooldown a las bolas de fuego
    IEnumerator FireTimeFalse(float f)
    {
        yield return new WaitForSeconds(f);

        fireTime = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //le baja vida al recibir daño  a traves del evento
        if (collision.transform.tag == "LuffyAttack" || collision.transform.tag == "pelota")
        {
            eventAtaqueSanji();
        }
        //si no hay una pelota en pantalla, instacia una
        else if (collision.transform.tag == "boton")
        {
            p = GameObject.Find("pelota(Clone)");
           
            if(p == null)
            {
                Instantiate(pelota);
            }
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //indica que esta en el suelo
        if (collision.gameObject.tag.Equals("Floor"))
        {
            isGrounded = true;
        }
        else if (collision.transform.tag.Equals("pelota"))
        {
            //empuja la pelota al golpearla
            if (side == true)
            {
                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(500, 0));
            }
            else
            {
                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-500, 0));
            }

        }
    }
    //activa la opcion de combear
    IEnumerator ComboHit(float f)
    {
        yield return new WaitForSeconds(f);
       
        if (!cBreak)
        {
            status = st;
        }
    }
    //desactiva la opcion de combear
    IEnumerator Combobreak(float f)
    {
        yield return new WaitForSeconds(f);
       
        if (status == st)
        {
            status = 0;
        }
    }
    //carga la pantalla final
    void cargarFinal()
    {
        SceneManager.LoadScene("WinLuffy");
    }
}
