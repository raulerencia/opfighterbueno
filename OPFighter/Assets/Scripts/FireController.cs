﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireController : MonoBehaviour
{
    
    GameObject sanji;
    GameObject luffy;
    SanjiController sc;
    // Start is called before the first frame update
    void Start()
    {
        
        sanji = GameObject.Find("Sanji");
        luffy = GameObject.Find("Luffy");
        this.gameObject.GetComponent<Transform>().position = sanji.gameObject.GetComponent<Transform>().position;
        
        //indica la direccion donde saldra la bola
        if(sanji.GetComponent<SanjiController>().side == true)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(800, 800));
        }
        else
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-800, 800));
        }
        //se destruye tras un tiempo
        Destroy(this.gameObject, 4);
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    //condiciones para destruirse
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag.Equals("LuffyBody"))
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag.Equals("Wall") || collision.transform.tag.Equals("pelota"))
        {
            Destroy(this.gameObject);
        }
    }
}
