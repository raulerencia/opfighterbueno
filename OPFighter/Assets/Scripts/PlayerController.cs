﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    //evento para poder restar la vida del marcador al recibir daño
    public delegate void ataqueLuffy();
    public event ataqueLuffy eventAtaqueLuffy;

    [Header("Valores")]
    public GameObject pj;
    public int velocity;
    public int jumpForce;
    public GameObject life;
    private int status = 0;
    private int st = 1;
    GameObject fireball;
    GameObject p;
    public GameObject pelota;

    [Header("Keys")]
    public KeyCode right;
    public KeyCode left;
    public KeyCode jump;
    public KeyCode hit;
    public KeyCode combo;
    public KeyCode guard;
    public KeyCode bite;

    [Header("Booleans")]
    public bool isGrounded;
    public bool side = true;
    private bool cBreak = false;
    public bool isGrabbing = false;
    public bool died = false;

    [Header("Animaciones")]
    public Animator animator;
   
    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        Move();
        fireball = GameObject.Find("fireball");
    }

    void Move()
    {
        //activa la animacion de protegerse y desactiva la hurtbox
        if (Input.GetKey(guard) && !isGrabbing && !died)
        {
            animator.SetBool("guard", true);
        }
        //da un golpe, esto activa dos corutinas, una para hacer el combo despues de 0,4seg, y cancelarlo despues de 1seg
        else if (Input.GetKeyDown(hit) && !isGrabbing && !died)
        {
            animator.SetTrigger("hit");
            cBreak = false;
            StartCoroutine(ComboHit(0.4f));
            StartCoroutine(Combobreak(1f));

        }
        //activa el combo si esta dentro del tiempo
        else if (Input.GetKeyDown(combo) && !isGrabbing && !died)
        {
            if (status == st)
            {
                animator.SetTrigger("puños");
            }
        }
        //activa la colision que empuja al rival.
        else if (Input.GetKeyDown(bite) && isGrounded && !isGrabbing && !died)
        {
            animator.SetTrigger("bite");
        }
        //movimientos laterales.
        else if (Input.GetKey(right) && !isGrabbing && !died)
        {
            if (isGrounded)
            {
                animator.SetBool("jump", false);
                animator.SetBool("left", false);
            }
            if (side == false)
            {
                side = true;

                this.transform.Rotate(0, 180, 0);
            }

            this.transform.Translate(new Vector3(0.1f, 0, 0));

            animator.SetBool("right", true);
        }
        else if (Input.GetKey(left) && !isGrabbing && !died)
        {
            if (isGrounded)
            {
                animator.SetBool("right", false);
                animator.SetBool("jump", false);
            }
            if (side == true)
            {
                side = false;

                this.transform.Rotate(0, 180, 0);
            }

            this.transform.Translate(new Vector3(0.1f, 0, 0));
            animator.SetBool("left", true);
        }
        //salto del personaje
        else if (Input.GetKey(jump) && isGrounded && !isGrabbing && !died)
        {
            animator.SetBool("right", false);
            animator.SetBool("left", false);
            animator.SetBool("jump", true);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
            isGrounded = false;
        }
        //si esta muerto carga la escena siguiente.
        else if (died)
        {
            animator.SetBool("died", true);
            Invoke("cargarFinal", 3);
        }
        //si no tiene vida, activa el bool "died"
        else if (life.GetComponent<Image>().fillAmount == 0)
        {
            animator.SetBool("die", true);
            if (side == true)
            {
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200, 700));
            }
            else
            {
                this.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(200, 700));
            }
            
            died = true;
            
        }
        else
        {
            //desactiva animaciones
            if (isGrounded)
            {
                animator.SetBool("right", false); animator.SetBool("jump", false); animator.SetBool("guard", false); animator.SetBool("left", false);
            }
            
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //activa el bool de tocar el suelo
        if (collision.gameObject.tag.Equals("Floor"))
        {
            isGrounded = true;
        }
        //empuja la pelota cuando le pegas.
        else if (collision.transform.tag.Equals("pelota"))
        {
            if (side == true)
            {
                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(500, 0));
            }
            else
            {
                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-500, 0));
            }
           
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //activa el evento de restar vida a Lufffy despues de haber sido golpeado
        if (collision.transform.tag == "SanjiAttack" || collision.transform.tag == "FireBall" || collision.transform.tag == "pelota")
        {
            eventAtaqueLuffy();
        }
        //instancia una pelota despues de tocar el boton, si ya hay una en pantalla no hace nada.
        else if (collision.transform.tag == "boton")
        {
            p = GameObject.Find("pelota(Clone)");
            if (p == null)
            {
                Instantiate(pelota);
                
            }
        }
    }
    //activa la opcion de combear
    IEnumerator ComboHit(float f)
    {
        yield return new WaitForSeconds(f);
        if (!cBreak)
        {
            status = st;            
        }
    }
    //desactiva la opcion de combear
    IEnumerator Combobreak(float f)
    {
        yield return new WaitForSeconds(f);

        if (status == st)
        {
            status = 0;
        }
    }
    //carga la pantalla final
    void cargarFinal()
    {
        SceneManager.LoadScene("WinSanji");
    }
}
