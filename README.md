Controles Luffy:
    Movimiento: WASD
    Golpe: H
    Combo despues del golpe: G
    Grab/Empuje: J
    Defensa: E
    
Controles Sanji:
     Movimiento: Flechitas
    Golpe: 1
    Combo despues del golpe: 4
    Bola de fuego: 2
    Defensa: 3

Los personajes pueden pegarse entre ellos y asimismo restarse la vida entre ellos.
Ademas, pueden tocar los botones que hay en las esquinas del mapa para que aparezca una pelota en el centro de la escena, 
esta puede ser golpeada por los jugadores para enviarsela al otro, si la pelota golpea a alguno de los dos personajes le restara vida a este.
En la otra escena los jugadores podran pegarse entre ellos y saltar entre las plataformas.